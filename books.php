<?php
  include "db.php";
  include "query.php";
?>

<html>
  <head>
  <title> Object Oriented PHP </title>
  </head>
  <body>
    <p>
    <?php
     
     $db = new DB('localhost','intro','root','');
     $dbc = $db->connect();
     $query = new Query($dbc);
     $q = "SELECT B.title, B.author, U.name
     FROM books B JOIN users U
     WHERE b.User_id=U.id";
     $result = $query->query($q);
     echo '<br>';
     //echo $result->num_rows;
     if($result->num_rows > 0){
       echo '<table>';
       echo '<tr><th>Book Title</th><th>Author</th><th>User Name</th></tr>';
       while($row=$result->fetch_assoc() ){
        echo '<tr>';
        echo '<td>'.$row['title'].'</td><td>'.$row['author'].'</td><td>'.$row['name'].'</td>';
        echo '</tr>';
       }
       echo '</table>';
     }
     else{
       echo "sorry no results";
     }

     ?>
    </p>
  </body>
</html>

