<?php
class Query {
    protected $dbc;
    function __Construct($dbc){
        $this->dbc = $dbc;
    }

    public function query($query){
        if($result = $this->dbc->query($query)){
            return($result);
        }
        else{
            echo "Something went wrong with the query";
        }
    }
}
?>
